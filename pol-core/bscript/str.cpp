/** @file
 *
 * @par History
 * - 2007/12/09 Shinigami: removed ( is.peek() != EOF ) check from String::unpackWithLen()
 *                         will not work with Strings in Arrays, Dicts, etc.
 * - 2008/02/08 Turley:    String::unpackWithLen() will accept zero length Strings
 * - 2009/09/12 MuadDib:   Disabled 4244 in this file due to it being on a string iter. Makes no
 * sense.
 */

#include "impstr.h"

#include "berror.h"
#include "bobject.h"
#include "objmethods.h"
#include "executor.h"
#include "../clib/stlutil.h"
#include <boost/algorithm/clamp.hpp>
#include <boost/algorithm/string.hpp>

#include <cstdlib>
#include <cstring>
#include "../clib/random.h"
#include "../clib/base64.cpp"

#include "../pol/polcfg.h"
#include "../plib/systemstate.h"
#include <boost/variant/detail/substitute.hpp>
#include <boost/variant/detail/substitute.hpp>
#include <boost/variant/detail/substitute.hpp>
#include <boost/variant/detail/substitute.hpp>

#ifdef __GNUG__
#include <streambuf>
#endif


const std::locale pol_locale(Pol::Plib::systemstate.config.locale);

#ifdef _MSC_VER
#pragma warning( disable : 4244 )
#endif
namespace Pol
{
       
namespace Bscript
{
String::String( BObjectImp& objimp ) : BObjectImp( OTString ), value_( objimp.getStringRep() )
{
}

String::String( const char* s, int len ) : BObjectImp( OTString ), value_( s, len )
{
}

String* String::StrStr( int begin, int len )
{
  return new String( value_.substr( begin - 1, len ) );
}

String* String::ETrim( const char* CRSet, int type )
{
  std::string tmp = value_;

  if ( type == 1 )  // This is for Leading Only.
  {
    // Find the first character position after excluding leading blank spaces
    size_t startpos = tmp.find_first_not_of( CRSet );
    if ( std::string::npos != startpos )
      tmp = tmp.substr( startpos );
    else
      tmp = "";
    return new String( tmp );
  }
  else if ( type == 2 )  // This is for Trailing Only.
  {
    // Find the first character position from reverse
    size_t endpos = tmp.find_last_not_of( CRSet );
    if ( std::string::npos != endpos )
      tmp = tmp.substr( 0, endpos + 1 );
    else
      tmp = "";
    return new String( tmp );
  }
  else if ( type == 3 )
  {
    // Find the first character position after excluding leading blank spaces
    size_t startpos = tmp.find_first_not_of( CRSet );
    // Find the first character position from reverse af
    size_t endpos = tmp.find_last_not_of( CRSet );

    // if all spaces or empty return on empty string
    if ( ( std::string::npos == startpos ) || ( std::string::npos == endpos ) )
      tmp = "";
    else
      tmp = tmp.substr( startpos, endpos - startpos + 1 );
    return new String( tmp );
  }
  else
    return new String( tmp );
}

void String::EStrReplace( String* str1, String* str2 )
{
  std::string::size_type valpos = 0;
  while ( std::string::npos != ( valpos = value_.find( str1->value_, valpos ) ) )
  {
    value_.replace( valpos, str1->length(), str2->value_ );
    valpos += str2->length();
  }
}

void String::ESubStrReplace( String* replace_with, unsigned int index, unsigned int len )
{
  value_.replace( index - 1, len, replace_with->value_ );
}

std::string String::pack() const
{
  return "s" + value_;
}

void String::packonto( std::ostream& os ) const
{
  os << "S" << value_.size() << ":" << value_;
}
void String::packonto( std::ostream& os, const std::string& value )
{
  os << "S" << value.size() << ":" << value;
}

BObjectImp* String::unpack( const char* pstr )
{
  return new String( pstr );
}

BObjectImp* String::unpack( std::istream& is )
{
  std::string tmp;
  getline( is, tmp );

  return new String( tmp );
}

BObjectImp* String::unpackWithLen( std::istream& is )
{
  unsigned len;
  char colon;
  if ( !( is >> len >> colon ) )
  {
    return new BError( "Unable to unpack string length." );
  }
  if ( (int)len < 0 )
  {
    return new BError( "Unable to unpack string length. Invalid length!" );
  }
  if ( colon != ':' )
  {
    return new BError( "Unable to unpack string length. Bad format. Colon not found!" );
  }

  is.unsetf( std::ios::skipws );
  std::string tmp;
  tmp.reserve( len );
  while ( len-- )
  {
    char ch = '\0';
    if ( !( is >> ch ) || ch == '\0' )
    {
      return new BError( "Unable to unpack string length. String length excessive." );
    }
    tmp += ch;
  }

  is.setf( std::ios::skipws );
  return new String( tmp );
}

size_t String::sizeEstimate() const
{
  return sizeof( String ) + value_.capacity();
}

/*
    0-based string find
    find( "str srch", 2, "srch"):
    01^-- start
    */
int String::find( int begin, const char* target )
{
  // TODO: check what happens in string if begin position is out of range
  std::string::size_type pos;
  pos = value_.find( target, begin );
  if ( pos == std::string::npos )
    return -1;
  else
    return static_cast<int>( pos );
}

// Returns the amount of alpha-numeric characters in string.
unsigned int String::alnumlen( void ) const
{
  unsigned int c = 0;
  while ( isalnum( value_[c] ) )
  {
    c++;
  }
  return c;
}

unsigned int String::SafeCharAmt() const
{
  int strlen = static_cast<int>( this->length() );
  for ( int i = 0; i < strlen; i++ )
  {
    unsigned char tmp = value_[i];
    if ( isalnum( tmp ) )  // a-z A-Z 0-9
      continue;
    else if ( ispunct( tmp ) )  // !"#$%&'()*+,-./:;<=>?@{|}~
    {
      if ( tmp == '{' || tmp == '}' )
        return i;
      else
        continue;
    }
    else
    {
      return i;
    }
  }
  return strlen;
}

void String::reverse( void )
{
  std::reverse( value_.begin(), value_.end() );
}

void String::set( char* newstr )
{
  value_ = newstr;
  delete newstr;
}


BObjectImp* String::selfPlusObjImp( const BObjectImp& objimp ) const
{
  return objimp.selfPlusObj( *this );
}
BObjectImp* String::selfPlusObj( const BObjectImp& objimp ) const
{
  return new String( value_ + objimp.getStringRep() );
}
BObjectImp* String::selfPlusObj( const BLong& objimp ) const
{
  return new String( value_ + objimp.getStringRep() );
}
BObjectImp* String::selfPlusObj( const Double& objimp ) const
{
  return new String( value_ + objimp.getStringRep() );
}
BObjectImp* String::selfPlusObj( const String& objimp ) const
{
  return new String( value_ + objimp.getStringRep() );
}
BObjectImp* String::selfPlusObj( const ObjArray& objimp ) const
{
  return new String( value_ + objimp.getStringRep() );
}
void String::selfPlusObjImp( BObjectImp& objimp, BObject& obj )
{
  objimp.selfPlusObj( *this, obj );
}
void String::selfPlusObj( BObjectImp& objimp, BObject& /*obj*/ )
{
  value_ += objimp.getStringRep();
}
void String::selfPlusObj( BLong& objimp, BObject& /*obj*/ )
{
  value_ += objimp.getStringRep();
}
void String::selfPlusObj( Double& objimp, BObject& /*obj*/ )
{
  value_ += objimp.getStringRep();
}
void String::selfPlusObj( String& objimp, BObject& /*obj*/ )
{
  value_ += objimp.getStringRep();
}
void String::selfPlusObj( ObjArray& objimp, BObject& /*obj*/ )
{
  value_ += objimp.getStringRep();
}


void String::remove( const char* rm )
{
  size_t len = strlen( rm );

  auto pos = value_.find( rm );
  if ( pos != std::string::npos )
    value_.erase( pos, len );
}

BObjectImp* String::selfMinusObjImp( const BObjectImp& objimp ) const
{
  return objimp.selfMinusObj( *this );
}
BObjectImp* String::selfMinusObj( const BObjectImp& objimp ) const
{
  String* tmp = (String*)copy();
  tmp->remove( objimp.getStringRep().data() );
  return tmp;
}
BObjectImp* String::selfMinusObj( const BLong& objimp ) const
{
  String* tmp = (String*)copy();
  tmp->remove( objimp.getStringRep().data() );
  return tmp;
}
BObjectImp* String::selfMinusObj( const Double& objimp ) const
{
  String* tmp = (String*)copy();
  tmp->remove( objimp.getStringRep().data() );
  return tmp;
}
BObjectImp* String::selfMinusObj( const String& objimp ) const
{
  String* tmp = (String*)copy();
  tmp->remove( objimp.value_.data() );
  return tmp;
}
BObjectImp* String::selfMinusObj( const ObjArray& objimp ) const
{
  String* tmp = (String*)copy();
  tmp->remove( objimp.getStringRep().data() );
  return tmp;
}
void String::selfMinusObjImp( BObjectImp& objimp, BObject& obj )
{
  objimp.selfMinusObj( *this, obj );
}
void String::selfMinusObj( BObjectImp& objimp, BObject& /*obj*/ )
{
  remove( objimp.getStringRep().data() );
}
void String::selfMinusObj( BLong& objimp, BObject& /*obj*/ )
{
  remove( objimp.getStringRep().data() );
}
void String::selfMinusObj( Double& objimp, BObject& /*obj*/ )
{
  remove( objimp.getStringRep().data() );
}
void String::selfMinusObj( String& objimp, BObject& /*obj*/ )
{
  remove( objimp.value_.data() );
}
void String::selfMinusObj( ObjArray& objimp, BObject& /*obj*/ )
{
  remove( objimp.getStringRep().data() );
}

bool String::operator==( const BObjectImp& objimp ) const
{
  if ( objimp.isa( OTString ) )
    return ( value_ == static_cast<const String&>( objimp ).value_ );

  return base::operator==( objimp );
}

bool String::operator<( const BObjectImp& objimp ) const
{
  if ( objimp.isa( OTString ) )
    return ( value_ < static_cast<const String&>( objimp ).value_ );

  return base::operator<( objimp );
}

String* String::midstring( int begin, int len ) const
{
  return new String( value_.substr( begin - 1, len ) );
}

void String::toUpper( void )
{
  for ( auto& c : value_ ) {
    //c = toupper( c ); // original POL99 GIT
    c = std::toupper(c, pol_locale);
  }
}

void String::toLower( void )
{
  for ( auto& c : value_ ) {
    //c = tolower( c );
    c = std::tolower(c, pol_locale);
  }
}

BObjectImp* String::array_assign( BObjectImp* idx, BObjectImp* target, bool /*copy*/ )
{
  std::string::size_type pos, len;

  // first, determine position and length.
  if ( idx->isa( OTString ) )
  {
    String& rtstr = (String&)*idx;
    pos = value_.find( rtstr.value_ );
    len = rtstr.length();
  }
  else if ( idx->isa( OTLong ) )
  {
    BLong& lng = (BLong&)*idx;
    pos = lng.value() - 1;
    len = 1;
  }
  else if ( idx->isa( OTDouble ) )
  {
    Double& dbl = (Double&)*idx;
    pos = static_cast<std::string::size_type>( dbl.value() );
    len = 1;
  }
  else
  {
    return UninitObject::create();
  }

  if ( pos != std::string::npos )
  {
    if ( target->isa( OTString ) )
    {
      String* target_str = (String*)target;
      value_.replace( pos, len, target_str->value_ );
    }
    return this;
  }
  else
  {
    return UninitObject::create();
  }
}

BObjectRef String::OperMultiSubscriptAssign( std::stack<BObjectRef>& indices, BObjectImp* target )
{
  BObjectRef start_ref = indices.top();
  indices.pop();
  BObjectRef length_ref = indices.top();
  indices.pop();

  BObject& length_obj = *length_ref;
  BObject& start_obj = *start_ref;

  BObjectImp& length = length_obj.impref();
  BObjectImp& start = start_obj.impref();

  // first deal with the start position.
  unsigned index;
  if ( start.isa( OTLong ) )
  {
    BLong& lng = (BLong&)start;
    index = (unsigned)lng.value();
    if ( index == 0 || index > value_.size() )
      return BObjectRef( new BError( "Subscript out of range" ) );
  }
  else if ( start.isa( OTString ) )
  {
    String& rtstr = (String&)start;
    std::string::size_type pos = value_.find( rtstr.value_ );
    if ( pos != std::string::npos )
      index = static_cast<unsigned int>( pos + 1 );
    else
      return BObjectRef( new UninitObject );
  }
  else
  {
    return BObjectRef( copy() );
  }

  // now deal with the length.
  int len;
  if ( length.isa( OTLong ) )
  {
    BLong& lng = (BLong&)length;

    len = (int)lng.value();
  }
  else if ( length.isa( OTDouble ) )
  {
    Double& dbl = (Double&)length;

    len = (int)dbl.value();
  }
  else
  {
    return BObjectRef( copy() );
  }

  if ( target->isa( OTString ) )
  {
    String* target_str = (String*)target;
    value_.replace( index - 1, len, target_str->value_ );
  }
  else
  {
    return BObjectRef( UninitObject::create() );
  }

  return BObjectRef( this );
}


BObjectRef String::OperMultiSubscript( std::stack<BObjectRef>& indices )
{
  BObjectRef start_ref = indices.top();
  indices.pop();
  BObjectRef length_ref = indices.top();
  indices.pop();

  BObject& length_obj = *length_ref;
  BObject& start_obj = *start_ref;

  BObjectImp& length = length_obj.impref();
  BObjectImp& start = start_obj.impref();

  // first deal with the start position.
  unsigned index;
  if ( start.isa( OTLong ) )
  {
    BLong& lng = (BLong&)start;
    index = (unsigned)lng.value();
    if ( index == 0 || index > value_.size() )
      return BObjectRef( new BError( "Subscript out of range" ) );
  }
  else if ( start.isa( OTString ) )
  {
    String& rtstr = (String&)start;
    std::string::size_type pos = value_.find( rtstr.value_ );
    if ( pos != std::string::npos )
      index = static_cast<unsigned int>( pos + 1 );
    else
      return BObjectRef( new UninitObject );
  }
  else
  {
    return BObjectRef( copy() );
  }

  // now deal with the length.
  int len;
  if ( length.isa( OTLong ) )
  {
    BLong& lng = (BLong&)length;

    len = (int)lng.value();
  }
  else if ( length.isa( OTDouble ) )
  {
    Double& dbl = (Double&)length;

    len = (int)dbl.value();
  }
  else
  {
    return BObjectRef( copy() );
  }

  auto str = new String( value_, index - 1, len );
  return BObjectRef( str );
}

BObjectRef String::OperSubscript( const BObject& rightobj )
{
  const BObjectImp& right = rightobj.impref();
  if ( right.isa( OTLong ) )
  {
    auto& lng = (BLong&)right;

    auto index = static_cast<unsigned>(lng.value());

    if ( index == 0 || index > value_.size() )
      return BObjectRef( new BError( "Subscript out of range" ) );

    return BObjectRef( new BObject( new String( value_.c_str() + index - 1, 1 ) ) );
  }
  else if ( right.isa( OTDouble ) )
  {
    Double& dbl = (Double&)right;

    unsigned index = (unsigned)dbl.value();

    if ( index == 0 || index > value_.size() )
      return BObjectRef( new BError( "Subscript out of range" ) );

    return BObjectRef( new BObject( new String( value_.c_str() + index - 1, 1 ) ) );
  }
  else if ( right.isa( OTString ) )
  {
    auto& rtstr = (String&)right;
    auto pos = value_.find( rtstr.value_ );
    if ( pos != std::string::npos )
      return BObjectRef( new BObject( new String( value_, pos, 1 ) ) );
    else
      return BObjectRef( new UninitObject );
  }
  else
  {
    return BObjectRef( new UninitObject );
  }
}

// -- format related stuff --

bool s_parse_int( int& i, std::string const& s )
{
  if ( s.empty() )
    return false;

  char* end;
  i = strtol( s.c_str(), &end, 10 );

  if ( !*end )
  {
    return true;
  }
  return false;
}

// remove leading/trailing spaces
void s_trim( std::string& s )
{
  std::stringstream trimmer;
  trimmer << s;
  s.clear();
  trimmer >> s;
}

void int_to_binstr( int& value, std::stringstream& s )
{
  int i;
  for ( i = 31; i > 0; i-- ) {
    if ( value & ( 1 << i ) )
      break;
  }
  for ( ; i >= 0; i-- ) {
    if ( value & ( 1 << i ) )
      s << "1";
    else
      s << "0";
  }
}

// suplementory function to format
bool try_to_format( std::stringstream& to_stream, BObjectImp* what, std::string& frmt )
{
  if ( frmt.empty() )
  {
    to_stream << what->getStringRep();
    return false;
  }

  if ( frmt.find( 'b' ) != std::string::npos )
  {
    if ( !what->isa( BObjectImp::OTLong ) ) {
      to_stream << "<needs Int>";
      return false;
    }
    auto plong = static_cast<BLong*>( what );
    auto n = plong->value();
    if ( frmt.find( '#' ) != std::string::npos )
      to_stream << ( ( n < 0 ) ? "-" : "" ) << "0b";
    int_to_binstr( n, to_stream );
  }
  else if ( frmt.find( 'x' ) != std::string::npos )
  {
    if ( !what->isa( BObjectImp::OTLong ) ) {
      to_stream << "<needs Int>";
      return false;
    }
    auto plong = static_cast<BLong*>( what );
    auto n = plong->value();
    if ( frmt.find( '#' ) != std::string::npos )
      to_stream << "0x";
    to_stream << std::hex << n << std::dec;
  }
  else if ( frmt.find( 'o' ) != std::string::npos )
  {
    if ( !what->isa( BObjectImp::OTLong ) )
    {
      to_stream << "<needs Int>";
      return false;
    }
    auto plong = static_cast<BLong*>( what );
    auto n = plong->value();
    if ( frmt.find( '#' ) != std::string::npos )
      to_stream << "0o";
    to_stream << std::oct << n << std::dec;
  }
  else if ( frmt.find( 'd' ) != std::string::npos )
  {
    int n;
    if ( what->isa( BObjectImp::OTLong ) )
    {
      auto plong = static_cast<BLong*>( what );
      n = plong->value();
    }
    else if ( what->isa( BObjectImp::OTDouble ) )
    {
      auto pdbl = static_cast<Double*>( what );
      n = static_cast<int>(pdbl->value());
    }
    else
    {
      to_stream << "<needs Int, Double>";
      return false;
    }
    to_stream << std::dec << n;
  }
  else
  {
    to_stream << "<bad format: " << frmt << ">";
    return false;
  }
  return true;
}
// --

// templated version of my_equal so it could work with both char and wchar_t
template<typename charT>
struct my_equal {
    my_equal(const std::locale& loc) : loc_(loc) {}
    bool operator()(charT ch1, charT ch2)
    {
        return std::toupper(ch1, loc_) == std::toupper(ch2, loc_);
    }
private:
    const std::locale& loc_;
};

// find substring (case insensitive)
template<typename T>
int ci_find_substr(const T& str1, const T& str2, const std::locale& loc = std::locale())
{
    typename T::const_iterator it = std::search(str1.begin(), str1.end(),
        str2.begin(), str2.end(), my_equal<typename T::value_type>(loc));
    if (it != str1.end()) return it - str1.begin();
    else return -1; // not found
}

BObjectImp* String::call_method( const char* methodname, Executor& ex )
{
  auto objmethod = getKnownObjMethod( methodname );
  if ( objmethod != nullptr )
    return this->call_method_id( objmethod->id, ex );
  return nullptr;
}
BObjectImp* String::call_method_id( const int id, Executor& ex, bool /*forcebuiltin*/ )
{
  switch ( id )
  {
  case MTH_LENGTH:
  {
    if ( ex.numParams() == 0 )
        return new BLong( static_cast<int>( value_.length() ) );
    return new BError( "string.length() doesn't take parameters." );
  }  
  case MTH_FIND:
  {
    if ( ex.numParams() > 2 )
      return new BError( "string.find(Search, [Start]) takes only two parameters" );
    if ( ex.numParams() < 1 )
      return new BError( "string.find(Search, [Start]) takes at least one parameter" );
    const char* s = ex.paramAsString( 0 );
    int d = 0;
    if ( ex.numParams() == 2 )
      d = ex.paramAsLong( 1 );
    int posn = find( d ? ( d - 1 ) : 0, s ) + 1;
    return new BLong( posn );
  }
  case MTH_UPPER:
  {
    if ( ex.numParams() == 0 )
    {
      toUpper();
      return this;
    }
    else
      return new BError( "string.upper() doesn't take parameters." );
  }
  case MTH_LOWER:
  {
    if ( ex.numParams() == 0 )
    {
      toLower();
      return this;
    }
    else
      return new BError( "string.lower() doesn't take parameters." );
  }
  case MTH_FORMAT:
  {
    if ( ex.numParams() > 0 )
    {
      // string s = this->getStringRep(); // string itself
      std::stringstream result;

      size_t tag_start_pos;  // the position of tag's start "{"
      size_t tag_stop_pos;   // the position of tag's end "}"
      size_t tag_dot_pos;

      int tag_param_idx;

      size_t str_pos = 0;               // current string position
      size_t next_param_idx = 0;  // next index of .format() parameter

      char w_spaces[] = "\t ";

      // Tells whether last found tag was an integer
      // bool last_tag_was_int = true;

      while ( ( tag_start_pos = value_.find( '{', str_pos ) ) != std::string::npos ) {
        if ( ( tag_stop_pos = value_.find( '}', tag_start_pos ) ) != std::string::npos ) {

          result << value_.substr( str_pos, tag_start_pos - str_pos );
          str_pos = tag_stop_pos+1;

          auto tag_body =
            value_.substr( tag_start_pos+1, (tag_stop_pos - tag_start_pos) - 1 );

          tag_start_pos = tag_body.find_first_not_of( w_spaces );
          tag_stop_pos = tag_body.find_last_not_of( w_spaces );

          // trim the tag of whitespaces (slightly faster code ~25%)
          if ( tag_start_pos != std::string::npos && tag_stop_pos != std::string::npos )
            tag_body = tag_body.substr( tag_start_pos, ( tag_stop_pos - tag_start_pos ) + 1 );
          else if ( tag_start_pos != std::string::npos )
            tag_body = tag_body.substr( tag_start_pos );
          else if ( tag_stop_pos != std::string::npos )
            tag_body = tag_body.substr( 0, tag_stop_pos + 1 );

          std::string frmt;
          auto formatter_pos = tag_body.find( ':' );

          if ( formatter_pos != std::string::npos ) {
            frmt = tag_body.substr( formatter_pos+1, std::string::npos );
            tag_body = tag_body.substr( 0, formatter_pos );  // remove property from the tag
          }

          std::string prop_name;
          // parsing {1.this_part}
          tag_dot_pos = tag_body.find( '.', 0 );

          // '.' is found within the tag, there is a property name
          if ( tag_dot_pos != std::string::npos ) {
//            last_tag_was_int = true;
            prop_name = tag_body.substr( tag_dot_pos+1, std::string::npos );  //
            tag_body = tag_body.substr( 0, tag_dot_pos );  // remove property from the tag

            // if s_tag_body is numeric then use it as an index
            if ( s_parse_int( tag_param_idx, tag_body ) ) {
              tag_param_idx -= 1;  // sinse POL indexes are 1-based
            }
            else {
              result << "<idx required before: '" << prop_name << "'>";
              continue;
            }
          }
          else
          {
              if (s_parse_int(tag_param_idx, tag_body)) {
                  tag_param_idx -= 1; // sinse POL indexes are 1-based
              } else { // non-integer body has just next idx in line
                  prop_name = tag_body;
                  tag_param_idx = next_param_idx++;
              }
          }

          // -- end of property parsing

          // cout << "prop_name: '" << prop_name << "' tag_body: '" << tag_body << "'";

          if ( static_cast<int>(ex.numParams()) <= tag_param_idx ) {
            result << "<invalid index: #" << ( tag_param_idx + 1 ) << ">";
            continue;
          }

          auto imp = ex.getParamImp( tag_param_idx );

          if ( !prop_name.empty() ) {  
              // accesing object
              auto obj_member = imp->get_member(prop_name.c_str());
              auto member_imp = obj_member->impptr();
              try_to_format(result, member_imp, frmt);
          }
          else {
            try_to_format( result, imp, frmt );
          }

        } else {
          break;
        }
      }

      if ( str_pos < value_.length() )
      {
        result << value_.substr( str_pos, std::string::npos );
      }

      return new String( result.str() );
    }
        
    return new BError( "string.format() requires a parameter." );    
  }
  case MTH_EQUALS_IC:
  {
      const String* equals_to;
      if (ex.numParams() == 1 && ex.getStringParam(0, equals_to) != false) {

          if(boost::iequals(value_, equals_to->getStringRep(), pol_locale))
              return new BLong(1);

          /*if ((this->length() == equals_to->length()) && (ci_find_substr(value_, equals_to->getStringRep(), pol_locale) == 0)) {
              return new BLong(1);
          }*/

          return new BLong(0);
      }
      return new BError("string.equals_ic(equals_to:string) requires a parameter.");
  }
  case MTH_STARTSWITH:
  {
      if (ex.numParams() == 1) {
          const String* bimp_substr;
          if (!ex.getStringParam(0, bimp_substr)) {
              return new BError("Invalid parameter type.");
          }
          auto subsr = bimp_substr->getStringRep();
          if (this->getStringRep().find(subsr) == 0) {
              return new BLong(1);
          }
          return new BLong(0);
      }
        return new BError("string.startswith(s:string) takes exactly one parameter.");
    }
  case MTH_STARTSWITH_IC:
  {
      if (ex.numParams() == 1) {
          const String* bimp_substr;
          if (!ex.getStringParam(0, bimp_substr))
              return new BError("Invalid parameter type.");
          
          auto subsr = bimp_substr->getStringRep();
          auto res = ci_find_substr(this->getStringRep(), subsr, pol_locale);

          if (res == 0)
              return new BLong(1);
          return new BLong(0);
      }
        return new BError("string.startswith_ic(s:string) takes exactly one parameter.");
    }
  case MTH_OBFUSCATE:
  {
      int percent_chance = 100;

      if (ex.numParams() > 0) {

          if (ex.getParam(0, percent_chance)) {
              boost::algorithm::clamp(percent_chance, 0, 100);
          } else {
              return new BError("Invalid parameter type");
          }
      }

      auto obfuscated = this->getStringRep();
      char replace_c;

      /* for (std::string::size_type i = 0; i < obfuscated.size(); ++i) { */
      for (char& obfuscated_c : obfuscated) {
          switch (obfuscated_c) {
          case 'e':
              replace_c = '�';
              break;
          case 'o':
              replace_c = '�';
              break;
          case 'a':
              replace_c = '�';
              break;
              //case 'n':
              //	obfuscated[i] = '�';
          case 'y':
              replace_c = '�';
              break;
              //case 'r':
              //	obfuscated[i] = '�';
              //case 'w':
              //	obfuscated[i] = '�';
              //case 'k':
              //	obfuscated[i] = '�';
          case 'p':
              replace_c = '�';
              break;
          case 'A':
              replace_c = '�';
              break;
          case 'X':
              replace_c = '�';
              break;
          case 'x':
              replace_c = 'x';
              break;
          case 'C':
              replace_c = '�';
              break;
          case 'c':
              replace_c = '�';
              break;
          default:
              replace_c = '\0';
              break;
          }

          if (replace_c && Clib::random_int(100) < percent_chance) {
              obfuscated_c = replace_c;
          }
              //obfuscated[i] = replace_c;          
      }

      return new String(obfuscated);
  }
  case MTH_BASE64_DECODE:
  {
    if (ex.numParams() == 0) {
        auto s = this->getStringRep();
        auto decode = base64_decode(s);
        return new String(decode);
    }
    return new BError("string.base64_decode() doesn't take parameters.");
  }
  case MTH_BASE64_ENCODE:
  {
      if (ex.numParams() == 0) {
          auto s = this->getStringRep();
          auto encoded = base64_encode(reinterpret_cast<const unsigned char*>(s.c_str()), this->length());
          return new String(encoded);
      }
    return new BError("string.base64_encode() doesn't take parameters.");
  }
  case MTH_CAPITALIZE:
  {
        char w_spaces[] = "\t \r\n";

        auto result(value_);

      size_t word_start_pos;
      //size_t word_stop_pos;
      size_t str_pos = 0; // current string position

      while ((word_start_pos = value_.find_first_not_of(w_spaces, str_pos)) != std::string::npos) {
          result[word_start_pos] = std::toupper(result[word_start_pos], pol_locale);
          str_pos = value_.find_first_of(w_spaces, word_start_pos + 1);
          if (str_pos == std::string::npos)
              break;
      }

      return new String(result);

      //while((tag_start_pos = value_.find("{", str_pos)) != string::npos) {
      //		if((tag_stop_pos=value_.find("}", tag_start_pos)) != string::npos) {
  }
  case MTH_PROCESS_GENDER_TAGS:
  {
        if (ex.numParams() > 2)
            return new BError("string.process_gender_tags(gender: int) takes only two parameters");
        if (ex.numParams() < 1)
            return new BError("string.process_gender_tags(gender: int) takes at least one parameter");

        const char GENDER_STR_TAG[] = "$g:";

        auto gender = ex.paramAsLong(0);

        boost::algorithm::clamp(gender, 0, 1);

        auto str_len = value_.length();

        if (!str_len)
            return new String(value_);

        auto gtag_len = strlen(GENDER_STR_TAG);
        size_t gtag_pos, gtag_end_pos;
        size_t str_pos = 0; // current string position	

        std::stringstream result;

      while ((gtag_pos = value_.find(GENDER_STR_TAG, str_pos)) != std::string::npos &&
          (gtag_end_pos = value_.find(";", gtag_pos + gtag_len)) != std::string::npos) {

          result << value_.substr(str_pos, gtag_pos - str_pos);
          str_pos = gtag_end_pos + 1;

          //std::string gtag_text = value_.substr(gtag_pos, gtag_end_pos-gtag_pos+1);
          auto payload = value_.substr(gtag_pos + gtag_len, gtag_end_pos - (gtag_pos + gtag_len));

          //cout << "payload: " << payload << ", gtag_pos: " << gtag_pos << ", gtag_end_pos: " << gtag_end_pos << std::endl;

          auto sep_pos = payload.find(":");
          if (sep_pos == std::string::npos)
              result << payload;
          else {
              if (gender == 0)
                  result << payload.substr(0, sep_pos);
              else
                  result << payload.substr(sep_pos + 1, std::string::npos);
          }
      }

      if (str_pos < value_.length()) {
          result << value_.substr(str_pos, std::string::npos);
      }

      return new String(result.str());

      //while((tag_start_pos = value_.find("{", str_pos)) != string::npos) {
      //		if((tag_stop_pos=value_.find("}", tag_start_pos)) != string::npos) {
  }
  case MTH_JOIN:
  {
    BObject* cont;
    if ( ex.numParams() == 1 && ( cont = ex.getParamObj( 0 ) ) != nullptr ) {
      if ( !( cont->isa( OTArray ) ) )
        return new BError( "string.join expects an array" );
        auto container = static_cast<ObjArray*>( cont->impptr() );
      // no empty check here on purpose
      OSTRINGSTREAM joined;
      auto first = true;
      for ( const BObjectRef& ref : container->ref_arr ) {
        if ( ref.get() ) {
          BObject* bo = ref.get();

          if ( bo == nullptr )
            continue;
          if ( !first )
            joined << value_;
          else
            first = false;
          joined << bo->impptr()->getStringRep();
        }
      }
      return new String( OSTRINGSTREAM_STR( joined ) );
    }
    return new BError( "string.join(array) requires a parameter." );
  }

  default:
    return nullptr;
  }
}
}
}
